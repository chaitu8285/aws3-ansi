pipeline {
 agent any
  
   stages {
    stage ('adding ssh key to known hosts') {
     steps {
      sh 'sudo ssh-keyscan 13.233.229.56 >> ~/.ssh/known_hosts'
     }
    }
    stage ('running tomcat ansi script') {
     steps {
      sh 'sudo ansible-playbook -i hostsfile ansi.yml'
     }
    }
   }
}
